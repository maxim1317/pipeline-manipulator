"""Tests for Manipulator class
"""
from ..src.manipulator import Manipulator

CONSUMER_IP = '127.0.0.1'
CONSUMER_TYPE = 'test'
PRODUCER_IP = '127.0.0.1'
PRODUCER_TYPE = 'test'


def test_manipulator():
    """Test basic creation
    """

    man = Manipulator(
        producer_ip=PRODUCER_IP,
        producer_type=PRODUCER_TYPE,
        consumer_ip=CONSUMER_IP,
        consumer_type=CONSUMER_TYPE
    )
    assert man.online() is True
