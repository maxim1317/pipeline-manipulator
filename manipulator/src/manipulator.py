""" manipulator.py
    Manipulator is needed for taking data from stream and
    giving it to ANN via MLS
"""
from .utils import setup_logger


class Manipulator(object):
    """Summary

    Attributes:
        consumer_ip (TYPE): Description
        consumer_type (TYPE): Description
        logger (TYPE): Description
        producer_ip (TYPE): Description
        producer_type (TYPE): Description
    """

    def __init__(
            self,
            producer_ip,
            producer_type,
            consumer_ip,
            consumer_type
    ):
        self.producer_ip = producer_ip
        self.producer_type = producer_type
        self.producer = self._get_producer()

        self.consumer_ip = consumer_ip
        self.consumer_type = consumer_type
        self.consumer = self._get_consumer()

        self.logger = setup_logger(
            'Manipulator {}-{}'.format(
                self.producer_type,
                self.consumer_type
            )
        )

    def _get_consumer(self):
        """Summary

        Args:
            ip (TYPE): Description
            type (TYPE): Description
        """
        from .interfaces import CONSUMERS
        from .utils import check_ip

        check_ip(self.consumer_ip)

        self.consumer_type = str(self.consumer_type).lower()

        interface = CONSUMERS.get(self.consumer_type, False)
        if not interface:
            raise NotImplementedError(
                'Consumer type {} is not implemented. \
                You can now use one of these types: {}'.format(
                    self.consumer_type,
                    list(CONSUMERS.keys())
                )
            )

        consumer = interface(
            ip=self.consumer_ip
        )

        return consumer

    def _get_producer(self):
        """Summary

        Args:
            ip (TYPE): Description
            type (TYPE): Description
        """
        from .interfaces import PRODUCERS
        from .utils import check_ip

        check_ip(self.producer_ip)

        self.producer_type = str(self.producer_type).lower()

        interface = PRODUCERS.get(self.producer_type, False)
        if not interface:
            raise NotImplementedError(
                """
                    Consumer type {} is not implemented.
                    You can now use one of these types: {}
                """.format(
                    self.producer_type,
                    list(PRODUCERS.keys())
                )
            )

        producer = interface(
            ip=self.producer_ip
        )

        return producer

    def online(self):
        """Checks if manipulator is online
        """
        if all(x is not None for x in [self.producer, self.consumer]):
            return True
        else:
            return False
