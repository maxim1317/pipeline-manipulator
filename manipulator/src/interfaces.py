""" interfaces.py
    Dictionary of available interfaces
"""


class Test(object):
    """Test interface"""

    def __init__(self, ip):
        self.ip = ip


CONSUMERS = {
    "test": Test
}

PRODUCERS = {
    "test": Test
}
