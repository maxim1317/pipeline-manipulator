#!/bin/sh

PROJECT=manipulator

mkinit --inplace --noattrs $PROJECT/ > /dev/null
mkinit --inplace --noattrs $PROJECT/src > /dev/null
mkinit --inplace --noattrs $PROJECT/tests > /dev/null