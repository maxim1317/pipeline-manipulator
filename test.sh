#! /bin/bash

PROJECT=manipulator

docstr-coverage --skipmagic --verbose=3 $PROJECT/ || true && \
pytest --cov=$PROJECT $PROJECT/tests/ && coverage html