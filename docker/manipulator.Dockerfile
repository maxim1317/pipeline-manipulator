FROM jjanzic/docker-python3-opencv

RUN pip3 install --no-cache \
    pylint \
    pytest \
    pytest-cov \
    pytest-sugar \
    coverage-badge \
    docstr-coverage \
    mkinit \
    tqdm \
    coloredlogs

COPY prepare.sh /manipulator/
COPY test.sh /manipulator/
COPY run.sh /manipulator/

WORKDIR /manipulator

COPY manipulator/ manipulator/

CMD bash run.sh
